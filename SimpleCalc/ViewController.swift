//
//  ViewController.swift
//  SimpleCalc
//
//  Created by Kyle Olsen on 2/18/16.
//  Copyright © 2016 Kyle Olsen. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet var calcText: UILabel!
    var calculator = Calculator()
    var numFormatter = NSNumberFormatter()
    var resetDisplay = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        calcText.text = "0"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func numButtonPressed(sender: UIButton) {
        if resetDisplay {
            calcText.text = nil
            resetDisplay = false
        }
        let ctext = calcText.text ?? "0"
        if sender.tag == 0 && ctext == "0" {
            return
        }
        calcText.text =
            ctext == "0" ?
                "\(sender.tag)" :
                ctext + "\(sender.tag)"
    }

    @IBAction func clearButtonPressed(sender: AnyObject) {
        calculator.clear()
        calcText.text = "0"
    }
    
    @IBAction func operationButtonPressed(sender: UIButton) {
        if let _ = calculator.leftOperand {
        } else {
            calculator.leftOperand = numFormatter.numberFromString(calcText.text!)!.integerValue
        }
        calculator.operation = Calculator.Operation(rawValue: sender.tag)
        resetDisplay = true
    }
    
    @IBAction func equalsButtonPressed(sender: AnyObject) {
        calculator.rightOperand = numFormatter.numberFromString(calcText.text!)!.integerValue
        if let r = calculator.calculate() {
            calcText.text = numFormatter.stringFromNumber(r)
            calculator.clear()
            resetDisplay = true
        }
    }
    
}

