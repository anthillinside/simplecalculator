//
//  Calculator.swift
//  SimpleCalc
//
//  Created by Kyle Olsen on 2/18/16.
//  Copyright © 2016 Kyle Olsen. All rights reserved.
//

import Foundation

class Calculator {
    
    enum Operation: Int {
        case MULTIPLY = 0
        case DIVIDE
        case ADD
        case SUBTRACT
    }
    
    var leftOperand: Int?
    var rightOperand: Int?
    var operation: Operation?

    func calculate() -> Int? {
        if let l = self.leftOperand, r = self.rightOperand, op = self.operation {
            switch op {
            case Operation.MULTIPLY:
                return l * r
            case Operation.DIVIDE:
                return l / r
            case Operation.ADD:
                return l + r
            case Operation.SUBTRACT:
                return l - r
            }
        } else {
            return nil
        }
    }
    
    func clear() {
        self.leftOperand = nil
        self.rightOperand = nil
        self.operation = nil
    }
}